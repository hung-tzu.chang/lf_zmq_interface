﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PrincetonInstruments.LightField.AddIns;

namespace LightField.AddIns.LF_ZMQ_Interface
{
    public class LF_ZMQMessage
    {
        string command { get; set; }
        string[] args { get; set; }
    }
}
