﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;
using PrincetonInstruments.LightField.AddIns;
//using PrincetonInstruments.LightField.
using System.Windows.Media.Converters;
using System.Data;
using System.CodeDom;

namespace LightFieldAddIns.LF_ZMQ_Interface
{
    /// <summary>
    /// Interaction logic for LF_ZMQ_InterfaceAddIn.xaml
    /// </summary>
    public partial class LF_ZMQ_InterfaceAddIn : UserControl
    {
        ILightFieldApplication application_;
        IExperiment experiment_; 
        IFileManager fileManager_;
        string server_address_;
        //RegionOfInterest troi;
        bool server_running, server_paused, initialized;
        Thread svthread;
        public LF_ZMQ_InterfaceAddIn(ILightFieldApplication app)
        {
            InitializeComponent();
            application_ = app; 
            experiment_ = application_.Experiment;
            fileManager_ = application_.FileManager;
            server_running = false;
            server_paused = false;
            initialized = false;
        }

        private void ZMQServerBox_Checked(object sender, RoutedEventArgs e)
        {
            //MessageBox.Show("Hello World");
            server_address_ = ZMQAddressBox.Text;
            if(server_address_==null || server_address_=="")
            {
                MessageBox.Show("Server address cannot be empty!");
                ZMQServerBox.IsChecked = false;
                return;
            }
            server_running = true;
            if (server_paused)
            {
                server_paused=false;
            }
            if (!initialized)
            {
                svthread = new Thread(new ThreadStart(RunServer));
                svthread.Start();
                initialized = true;
            }
            //RunServer();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            StopServer();
        }

        public void StopServer()
        {
            if (server_paused)
            {
                server_paused=false;
            }
            if (server_running)
            {
                server_running=false;
            }
            if(initialized)
            {
                svthread.Join();
            }
        }
        private void ZMQServerBox_Unchecked(object sender, RoutedEventArgs e)
        {
            server_paused = true;
            //svthread.Join();
            //svthread = null;
        }

        bool ValidateAcquisition()
        {
            IDevice camera = null;
            foreach (IDevice device in application_.Experiment.ExperimentDevices)
            {
                if (device.Type == DeviceType.Camera)
                    camera = device;
            }
            ///////////////////////////////////////////////////////////////////////
            if (camera == null)
            {
                MessageBox.Show("This sample requires a camera!");
                return false;
            }
            ///////////////////////////////////////////////////////////////////////
            if (!application_.Experiment.IsReadyToRun)
            {
                MessageBox.Show("The system is not ready for acquisition, is there an error?");
                return false;
            }
            return true;
        }

        private void RunServer()
        {
            var recvmsg = new List<string>();
            var server = new ResponseSocket();
            int numframes;
            int tmp, tmp1; float tmpf;
            long sumtmp, mintmp, maxtmp;
            server.Bind(server_address_);
            //Action<int> action = new Action<int>(SumMinMax);
            while (server_running)
            {
                /*while (server_paused)
                {
                    Thread.Sleep(1000);
                }*/
                if (server.TryReceiveMultipartStrings(timeout: TimeSpan.FromSeconds(1), ref recvmsg))
                {
                    numframes = recvmsg.Count;
                    if (numframes > 0 && ValidateAcquisition())
                    {
                        switch (recvmsg[0])
                        {
                            case "INT":
                                if (int.TryParse(recvmsg[1], out tmp))
                                {
                                    experiment_.SetValue(CameraSettings.ShutterTimingExposureTime, tmp);
                                    server.SendMoreFrame("E0").SendFrame(experiment_.GetValue(CameraSettings.ShutterTimingExposureTime).ToString());
                                } else
                                {
                                    MessageBox.Show("Wrong integration time input!");
                                    server.SendFrame("E1");
                                }
                                break;
                            case "FRM":
                                if (int.TryParse(recvmsg[1], out tmp))
                                {
                                    experiment_.SetValue(ExperimentSettings.AcquisitionFramesToStore, tmp);
                                    server.SendMoreFrame("E0").SendFrame(experiment_.GetValue(ExperimentSettings.AcquisitionFramesToStore).ToString());
                                }
                                else
                                {
                                    MessageBox.Show("Wrong number of frames input!");
                                    server.SendFrame("E1");
                                }
                                break;
                            case "ADC":
                                if (float.TryParse(recvmsg[1], out tmpf))
                                {
                                    experiment_.SetValue(CameraSettings.AdcSpeed, tmpf);
                                    server.SendMoreFrame("E0").SendFrame(experiment_.GetValue(CameraSettings.AdcSpeed).ToString());
                                }
                                else
                                {
                                    MessageBox.Show("Wrong ADC speed input!");
                                    server.SendFrame("E1");
                                }
                                break;
                            case "ACQ":
                                experiment_.Acquire();
                                while (experiment_.IsRunning)
                                {
                                    Thread.Sleep(50);
                                }
                                server.SendFrame("A0");
                                break;
                            case "CAP":
                                if (numframes < 2)
                                {
                                    tmp = 1;
                                }
                                else if (!int.TryParse(recvmsg[1], out tmp))
                                {
                                    tmp = 1;
                                }
                                experiment_.SetValue(ExperimentSettings.AcquisitionFramesToStore, tmp);
                                IImageDataSet datasettmp = experiment_.Capture(tmp);
                                while (experiment_.IsRunning)
                                {
                                    Thread.Sleep(50);
                                }
                                if (datasettmp.Frames != tmp)
                                {
                                    // Clean up the image data set                    
                                    datasettmp.Dispose();
                                    server.SendFrame("E1");
                                    MessageBox.Show("Capture failed!");
                                    throw new ArgumentException("Frames are not equal");
                                }
                                Array imageData = datasettmp.GetFrame(0, tmp - 1).GetData();
                                IImageData imageFrame = datasettmp.GetFrame(0, tmp - 1);
                                long[] data = new long[imageData.Length];
                                imageData.CopyTo(data, 0);
                                sumtmp = data.Sum(); 
                                mintmp = data.Min(); maxtmp = data.Max();
                                server.SendMoreFrame("C0").SendMoreFrame((sumtmp / data.Length).ToString()).SendMoreFrame(sumtmp.ToString()).SendMoreFrame(maxtmp.ToString()).SendFrame(mintmp.ToString());

                                //IDisplay display = application_.DisplayManager;
                                //display.ShowDisplay(DisplayLocation.ExperimentWorkspace,DisplayLayout.One);
                                IDisplayViewer viewer = application_.DisplayManager.GetDisplay(DisplayLocation.ExperimentWorkspace,0);
                                viewer.Display("cap", datasettmp);

                                datasettmp.Dispose();
                                break;
                            case "FSR":
                                experiment_.SetFullSensorRegion();
                                server.SendFrame("FSR");
                                break;
                            case "BSR":
                                if (numframes != 3)
                                {
                                    server.SendFrame("E1");
                                    MessageBox.Show("Wrong binning input!");
                                    break;
                                }
                                if (int.TryParse(recvmsg[1],out tmp) && int.TryParse(recvmsg[2],out tmp1))
                                {
                                    experiment_.SetBinnedSensorRegion(tmp, tmp1);
                                    server.SendFrame("BSR");
                                } else
                                {
                                    server.SendFrame("E1");
                                    MessageBox.Show("Wrong binning input!");
                                }
                                break;
                            case "ROI":
                                if (numframes != 7)
                                {
                                    server.SendFrame("E1");
                                    MessageBox.Show("Wrong ROI input!");
                                    break;
                                }
                                //tmp1 = 0;
                                int[] troi = new int[6];
                                tmp1 = 0;
                                for(int i=0; i<6; i++)
                                {
                                    if (int.TryParse(recvmsg[i+1],out tmp))
                                    {
                                        troi[i] = tmp;
                                    } else
                                    {
                                        tmp1 = 1;
                                        server.SendMoreFrame("E1").SendFrame(i.ToString());
                                        break;
                                    }
                                }
                                if (tmp1 == 0)
                                {
                                    RegionOfInterest ttroi = new RegionOfInterest(troi[0], troi[1], troi[2], troi[3], troi[4], troi[5]);
                                    RegionOfInterest[] a = { ttroi };
                                    experiment_.SetCustomRegions(a);
                                    server.SendFrame("ROI");
                                }
                                break;
                            default:
                                server.SendFrame("E1");
                                MessageBox.Show("Wrong Command!");
                                break;
                        }
                    } else
                    {
                        MessageBox.Show("Input command/camera error!");
                        server.SendFrame("E1");
                    }
                }
            }
        }
        private long ushorttolong(ushort val) { return ((long)val); }
        private long uinttolong(uint val) { return ((long)val); }
        private long inttolong(int val) { return ((long)val); }
        /*private void SumMinMax(int val)
        {
            sumtmp += val;
            if (val < mintmp)
            {
                mintmp = val;
            }
            if (val > maxtmp)
            {
                maxtmp = val;
            }
        }*/
    }    
}
