﻿using PrincetonInstruments.LightField.AddIns;
using System;
using System.AddIn;
using System.Windows;
using LightFieldAddIns;
// this file contains a pre-generated lightfield add-in
//
// to try out this add-in:
// - build the solution
// - run lightfield
// - activate the add-in via the Manage Add-ins... menu
// - invoke this add-in via the Add-ins menu
//
// to create additional add-ins use the lightfield code snippet:
// - add a new code file to this project
// - inside the editor type lfaddin and press tab

namespace LightFieldAddIns
{
    // TODO: edit the add-in description and publisher information
    [AddIn(
        "LF_ZMQ_Interface",
        Description = "Description Placeholder",
        Version = "1.0.0",
        Publisher = "Publisher Placeholder")]
    public class LF_ZMQ_InterfaceAddIn : AddInBase, ILightFieldAddIn
    {
        private LF_ZMQ_Interface.LF_ZMQ_InterfaceAddIn control_;
        public UISupport UISupport { get { return UISupport.ExperimentSetting; } }
        // this is called when lightfield loads this add-in
        public void Activate(ILightFieldApplication lightField)
        {
            // the following steps must happen first (in order)
            LightFieldApplication = lightField;
            //Initialize(Application.Current.Dispatcher, "LF_ZMQ_Interface");
            control_ = new LF_ZMQ_Interface.LF_ZMQ_InterfaceAddIn(LightFieldApplication);
            ExperimentSettingElement = control_;

            // TODO: add any initialization code
        }

        // this is called when lightfield unloads this add-in
        public void Deactivate()
        {
           control_.StopServer();
        }
        public override string UIExperimentSettingTitle { get { return "ZeroMQ Server"; } }

 /*
        #region Menu Interface

        // this tells lightfield to access this add-in via a menu
        public UISupport UISupport
        { get { return UISupport.Menu; } }

        // this tells lightfield the menu item invokes an action
        // (rather than keeping a checked/unchecked state)
        public override bool? UIMenuIsChecked
        {
            get { return null; }
            set { throw new NotSupportedException(); }
        }

        // this tells lightfield the menu item can be selected
        public override bool UIMenuIsEnabled
        { get { return true; } }

        // this tells lightfield the text to use for the menu item
        // TODO: edit the menu text
        public override string UIMenuTitle
        { get { return "Menu Title Placeholder"; } }

        // this is executed when the user selects the menu item
        // TODO: customize
        public override void UIMenuExecute()
        {
            MessageBox.Show("Hello World!");
        }

        #endregion
*/
    }
}